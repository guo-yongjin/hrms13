# -*- coding: utf-8 -*-
{
    'name': "自动备份",

    'summary': '数据库自动备份',

    'description': """自动备份数据库在本机或远程机器。
    然后，可以按如下方式为所有这些已配置的数据库安排自动备份：


    1） 转到设置/技术/自动化/计划操作。
    2） 搜索操作‘备份计划程序’。
    3） 将其设置为活动状态并选择备份的频率。
    4） 如果要将备份写入远程位置，应填写SFTP详细信息。
""",

    'website': "http:/www.redtn.cn",
    'category': 'Administration',
    'version': '13.0.0.1',
    'license': 'LGPL-3',

    'depends': ['base'],

    'data': [
        'security/user_groups.xml',
        'security/ir.model.access.csv',
        'views/backup_view.xml',
        'data/backup_data.xml',
    ],
    'installable': True,
}
