# -*- coding: utf-8 -*-
###################################################################################
#   
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################

{
    'name': "文档自定义",


    'author': "guo yongjin",
    'description': '''定制文档''',
    'website': "http://www.redtn.cn",

    'category': 'Document Management',
    'version': '13.0.1',

    'depends': ['base',
    'muk_dms',
    'muk_web_preview_image','muk_web_preview_text','muk_web_preview_video','muk_web_preview_msoffice'
    ],

    'data': [

        'views/config_settings_view.xml',
    ],
    
    'qweb': [
        'static/src/xml/base.xml',
    ],
       'images': ['static/description/logo.png'],

    'demo': [
    ],
    
     'installable': True,
    'auto_install': False,
    'licence': 'Affero GPL-3',
}
