# -*- coding: utf-8 -*-
###################################################################################
#   
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################
from odoo import models, fields


class SurveyInput(models.Model):
    _inherit = 'survey.user_input'

    appraisal_id = fields.Many2one('hr.appraisal', string="Appriasal id")
    emp_name = fields.Char(string='appraisaled employee', related='appraisal_id.emp_id.name',store= True)
    
class AppraisalSurvey(models.Model):
    _inherit = 'survey.survey'

    category = fields.Selection(selection_add=[('hr_appraisal', 'Appraisal')])

class SurveyLabel(models.Model):
    """ A suggested answer for a question """
    _inherit = 'survey.label'

    survey_title = fields.Char(string='survey title', related='question_id.survey_id.title',store= True)
