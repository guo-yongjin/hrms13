# -*- coding: utf-8 -*-
###################################################################################
#   
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################
{
    'name': "综合评估",
    'version': '13.0.1.0.0',
    'summary': """业绩和调查综合评估""",
    'description': """业绩和调查综合评估""",
    'category': 'Human Resources',
    'author': 'guo yongjin',
    'company': 'redtn.cn',
    'website': "https://www.redtn.cn",
    'depends': ['base', 'hr','hr_contract', 'survey','hr_appraisal','hr_appraisal_rule',],
    'data': [
        'security/ir.model.access.csv',
        #'views/hr_appraisal_survey_views.xml',
        'views/hr_appraisal_comb_view.xml',
    ],
    'images': ["static/description/banner.jpg"],
    'license': "AGPL-3",
    'installable': True,
    'application': False,
}
