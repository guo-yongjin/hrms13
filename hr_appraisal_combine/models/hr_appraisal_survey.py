# -*- coding: utf-8 -*-
###################################################################################
#   
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################
from odoo import models, fields, api


class CombSurveyInput(models.Model):
    _inherit = 'survey.user_input'

    appraisal_score = fields.Float("Score (%)", compute="_compute_appraisal_score", store=True, compute_sudo=True)
    
    @api.depends('user_input_line_ids.answer_score', 'user_input_line_ids.question_id')
    def _compute_appraisal_score(self):
        for user_input in self:
            total_possible_score = sum([
                answer_score if answer_score > 0 else 0
                for answer_score in user_input.user_input_line_ids.mapped('answer_score')
            ])
            user_input.appraisal_score = total_possible_score
