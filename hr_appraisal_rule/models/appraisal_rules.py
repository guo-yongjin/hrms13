# -*- coding: utf-8 -*-

from odoo import api, exceptions, fields, models, _
from odoo.exceptions import UserError, ValidationError


class appraisalrule(models.Model):
    _name = "hr.appraisal.rule"
    _description = "appraisal rule"

    def _default_company(self):
        return self.env.user.company_id

    name = fields.Char('Name',required=True)
    rule_type = fields.Selection(selection=[("sale","sale person"),("task","task deviation")],string="rule type",default = "sale")
    sale_amount_on = fields.Boolean(string = "appraisal by sale amount ",default = True)
    sale_received_on = fields.Boolean(string = "appraisal by received payments",default = False)
    task_deviation_on = fields.Boolean(string = "appraisal by task deviation",default = True)
    task_work_hours_on = fields.Boolean(string = "appraisal by work hours",default = False)
    amount_rule_line_ids = fields.One2many("hr.appraisal.amount.line","rule_id",string = "appraisal by amount")
    received_rule_line_ids = fields.One2many("hr.appraisal.received.line","rule_id",string = "appraisal by received")    
    deviation_rule_line_ids = fields.One2many("hr.appraisal.deviation.line","rule_id",string = "appraisal by deviation")
    workhour_rule_line_ids = fields.One2many("hr.appraisal.workhour.line","rule_id",string = "appraisal by workhour")
    sale_amount_ratio = fields.Integer(string = "ratio of sale amount ")
    sale_received_ratio = fields.Integer(string = "ratio of  received payments")
    task_deviation_ratio = fields.Integer(string = "ratio of  task deviation")
    task_work_hours_ratio = fields.Integer(string = "ratio of  work hours")
    active = fields.Boolean(string = "actived",default = True)
    company_id = fields.Many2one(
        comodel_name='res.company',
        required=True,  default=_default_company
    )

    @api.constrains('sale_amount_ratio','sale_received_ratio','task_deviation_ratio','task_work_hours_ratio')
    def _chech_ratio(self):
        for rule in self:
            if rule.sale_amount_ratio < 0 or rule.sale_amount_ratio > 100:
                raise ValidationError(_('You cannot have a  rule that ratio is out of range. (0--100)'))
            if rule.sale_received_ratio < 0 or rule.sale_received_ratio > 100:
                raise ValidationError(_('You cannot have a  rule that ratio is out of range. (0--100)'))
            if rule.task_deviation_ratio < 0 or rule.task_deviation_ratio > 100:
                raise ValidationError(_('You cannot have a  rule that ratio is out of range. (0--100)'))
            if rule.task_work_hours_ratio < 0 or rule.task_work_hours_ratio > 100:
                raise ValidationError(_('You cannot have a  rule that ratio is out of range. (0--100)'))

        
    
class RuleLine(models.Model):
    _name = "hr.appraisal.amount.line"
    _description = "rule line about sale amount"
    _sql_constraints = [
        ('check_floor', 'CHECK(floor >= 0  )', 'floor must larger than or equal to 0!'),
        ('check_grade', 'CHECK(grade >= 0 and grade <= 10)', 'grade must Between 0 and 10!'),
    ]

    rule_id = fields.Many2one(
        "hr.appraisal.rule",  ondelete="cascade",
        required=True )
    
    floor = fields.Integer(string = "lower limit")
#    roof = fields.Integer(string = "top limit")
    grade = fields.Float(string = "ratio for grade")
    order = fields.Integer(string = "rule line order numer")

class RuleLine1(models.Model):
    _name = "hr.appraisal.received.line"
    _description = "rule line about received payments"
    _sql_constraints = [
        ('check_floor', 'CHECK(floor >= 0  )', 'floor must larger than or equal to 0!'),
        ('check_grade', 'CHECK(grade >= 0 and grade <= 10)', 'grade must Between 0 and 10!'),
    ]

    rule_id = fields.Many2one(
        "hr.appraisal.rule",  ondelete="cascade",
        required=True )
    
    floor = fields.Integer(string = "lower limit")
#    roof = fields.Integer(string = "top limit")
    grade = fields.Float(string = "ratio for grade")
    order = fields.Integer(string = "rule line order numer")

class RuleLine2(models.Model):
    _name = "hr.appraisal.deviation.line"
    _description = "rule line about task deviation"
    _sql_constraints = [
        ('check_floor', 'CHECK(floor >= 0  )', 'floor must larger than or equal to 0!'),
        ('check_grade', 'CHECK(grade >= 0 and grade <= 10)', 'grade must Between 0 and 10!'),
    ]

    rule_id = fields.Many2one(
        "hr.appraisal.rule",  ondelete="cascade",
        required=True )
    
    floor = fields.Integer(string = "lower limit")
#    roof = fields.Integer(string = "top limit")
    grade = fields.Float(string = "ratio for grade")
    order = fields.Integer(string = "rule line order numer")
	
class RuleLine3(models.Model):
    _name = "hr.appraisal.workhour.line"
    _description = "rule line about task work hours"
    _sql_constraints = [
        ('check_floor', 'CHECK(floor >= 0  )', 'floor must larger than or equal to 0!'),
        ('check_grade', 'CHECK(grade >= 0 and grade <= 10)', 'grade must Between 0 and 10!'),
    ]

    rule_id = fields.Many2one(
        "hr.appraisal.rule",  ondelete="cascade",
        required=True )
    
    floor = fields.Integer(string = "lower limit")
#    roof = fields.Integer(string = "top limit")
    grade = fields.Float(string = "ratio for grade")
    order = fields.Integer(string = "rule line order numer")
	