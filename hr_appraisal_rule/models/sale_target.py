from odoo import models,fields,api

class SalePerson(models.Model):
    _name = "hr.appraisal.target"
    _description = "sale penson targets"
    _sql_constraints = [
        ('check_sale_received_amount', 'CHECK(sale_amount >= 0 )', 'sale_amount must larger than 0!'),
        ('check_sale_received', 'CHECK(sale_received >= 0 )', 'sale_received must larger than 0!'),
    ]

	
    sale_person = fields.Many2one("hr.employee",string = "employee from sale team",required = True)
    currency_id = fields.Many2one("res.currency", string="Currency")
    sale_amount = fields.Monetary('contract sum', digits=(16, 2), required=True, help="sale person's contract sum.")
    sale_received = fields.Monetary('received payments', digits=(16, 2), required=True, help="sale person's received payments.")