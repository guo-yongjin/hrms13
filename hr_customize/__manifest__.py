# -*- coding: utf-8 -*-
###################################################################################
#   
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################

{
    'name': 'Human Resource自定义',
    'version': '13.0.0.0',
    'author': 'guo yongjin ,redtn.cn',
    "category": "Human Resources",
    "description": """
        更适宜的人力资源各模块，招聘、入职 、绩效评估、工资、培训等。
    """,
    'license':'OPL-1',
    'summary': '提供人力资源主要模块功能',
    'website': 'www.redtn.cn',
    'depends':['l10n_cn_ext','employee_orientation','hr_org_chart','hr_skills','hr_attendance','hr_expense','hr_recruitment','survey','hr_recruitment_survey', 'hr_holidays','hr_presence','hr_timesheet',
            'hr_payroll_cn',
	    'hr_appraisal','hr_appraisal_rule','hr_appraisal_combine','hr_overtime_auto',
            'hr_resignation','hr_official_docs',
            
            ],
    'data':[
        'views/app_theme_config_settings_view.xml',
        'views/ui_views.xml',
        'views/scores_employee.xml',
        'data/employee_orientation_data.xml',
        'security/ir.model.access.csv',
        'data/mail_template_data.xml',
        ],
    'installable': True,
    'auto_install': False,
    'application': False,
    "images":['static/description/icon.png'],
}

