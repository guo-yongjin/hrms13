# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.osv import osv
from datetime import datetime
import time
from odoo.tools.translate import _
from odoo.exceptions import ValidationError,Warning

import logging
_logger = logging.getLogger(__name__)

class EmployeeScore(models.Model):
    _name = 'employee.score'
    _inherit = ['mail.thread' ]
    _description = 'the training score of employees'
    
    the_train = fields.Many2one('employee.training',string = 'the train' ,require=True)
    #training_ids = fields.Many2many('employee.training','employee_score_training_ref','score_id','train_id',string = 'train id' ,require=True)
    emp = fields.Many2one('hr.employee',string = 'training attendance' ,require = True)
    score = fields.Float(string = 'training score')
    scorer = fields.Many2one('res.users',string = 'trainees scorer')
    
