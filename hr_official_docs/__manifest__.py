# -*- coding: utf-8 -*-
###################################################################################
#   
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################

{
    "name": "公文",
    "summary": """简约公文系统""",
    "version": "13.0.1.1.17",
    "author": "guo yongjin",
    "category": "Human Resources/Official Documents",
    "license": "AGPL-3",
    "website": "http://www.redtn.cn",
    "depends": [
        "mail",
    ],
    "contributors": [
        "guo yongjin<guoyongjin1010@163.com>",
    ],
    "data": [
        "security/security.xml",
        "security/ir.model.access.csv",
        "template/assets.xml",
        "views/document_view.xml",
        "views/stage_view.xml",
        "views/user_stage_view.xml",
        #"views/groups_view.xml",
        "views/template_view.xml",
        "views/user_template_view.xml",
        "views/res_config_view.xml",
        "views/menu.xml",
        "data/stages.xml",
        "data/template.xml",
        #"data/refresh_actions.xml",
    ],
    "demo": [
    ],
    "qweb": [
        "static/src/xml/*.xml",
    ],
    "images": [
        'static/description/banner.png'
    ],
    "external_dependencies": {
        "python": [],
        "bin": [],
    },
    "sequence": 100,
    "installable": True,
    "auto_install": False,
    "application": True,
}
