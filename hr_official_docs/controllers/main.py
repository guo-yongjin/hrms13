# -*- coding: utf-8 -*-
###################################################################################
#   
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################

import logging

from odoo import _, http
from odoo.http import request

_logger = logging.getLogger(__name__)

class QualityController(http.Controller):

    @http.route('/sidebar/hr_official_docs_document/kanban', type='json', auth="user")
    def sidebar_document_kanban(self, **kw):
        if request.env.user.has_group('hr_official_docs.group_hr_official_docs_manager'):
            actions={
                'actions': [
                    {
                        'id': 'all',
                        'tooltip': _("All"),
                        'action': request.env.ref('hr_official_docs.documents_all_kanban').id,
                        'icon': "fa fa-list",
                        'badge': request.env['hr_official_docs.document'].search([['stage_id.next_stage_id','!=',False]], count=True) or 0,
                    }, {
                        'id': 'inbox',
                        'tooltip': _("Inbox"),
                        'action': request.env.ref('hr_official_docs.documents_unread_kanban').id,
                        'icon': "fa fa-inbox",
                        #'badge': request.env['hr_official_docs.document'].search([['stage_id.operator', '=', request.env.uid],['stage_id.iscompleted','=',False]], count=True) or 0,
                        'badge': request.env['hr_official_docs.document'].search([['stage_id.operator', '=', request.env.uid]], count=True) or 0,
                    }, {
                        'id': 'editor',
                        'tooltip': _("Editor"),
                        'action': request.env.ref('hr_official_docs.documents_editor_kanban').id,
                        'icon': "fa fa-pencil",
                        'badge': request.env['hr_official_docs.document'].search([['create_uid', '=', request.env.uid],['stage_id.prev_stage_id','=',False]], count=True) or 0,
                    }, {
                        'id': 'partner',
                        'tooltip': _("Partner"),
                        'action': request.env.ref('hr_official_docs.documents_partner_kanban').id,
                        'icon': "fa fa-user",
                        'badge': request.env['hr_official_docs.document'].search([['partner_id', '=', request.env.uid],['stage_id.next_stage_id','!=',False]], count=True) or 0,
                    }
                ]
            }
        else:
            actions={
                'actions': [
                    {
                        'id': 'inbox',
                        'tooltip': _("Inbox"),
                        'action': request.env.ref('hr_official_docs.documents_unread_kanban').id,
                        'icon': "fa fa-inbox",
                        #'badge': request.env['hr_official_docs.document'].search([['stage_id.operator', '=', request.env.uid],['stage_id.iscompleted','=',False]], count=True) or 0,
                        'badge': request.env['hr_official_docs.document'].sudo().search([['stage_id.operator', '=', request.env.uid]], count=True) or 0,
                    }, {
                        'id': 'editor',
                        'tooltip': _("Editor"),
                        'action': request.env.ref('hr_official_docs.documents_editor_kanban').id,
                        'icon': "fa fa-pencil",
                        'badge': request.env['hr_official_docs.document'].search([['create_uid', '=', request.env.uid],['stage_id.prev_stage_id','=',False]], count=True) or 0,
                    }, {
                        'id': 'partner',
                        'tooltip': _("Partner"),
                        'action': request.env.ref('hr_official_docs.documents_partner_kanban').id,
                        'icon': "fa fa-user",
                        'badge': request.env['hr_official_docs.document'].sudo().search([['partner_id', '=', request.env.uid],['stage_id.next_stage_id','!=',False]], count=True) or 0,
                    }
                ]
            }
        return actions

    @http.route('/sidebar/hr_official_docs_document/list', type='json', auth="user")
    def sidebar_document_list(self, **kw):
        if request.env.user.has_group('hr_official_docs.group_hr_official_docs_manager'):
            actions={
                'actions': [
                {
                    'id': 'all',
                    'tooltip': _("All"),
                    'action': request.env.ref('hr_official_docs.documents_all_list').id,
                    'icon': "fa fa-list",
                    'badge': request.env['hr_official_docs.document'].search([['stage_id.next_stage_id','!=',False]], count=True) or 0,
                }, {
                    'id': 'inbox',
                    'tooltip': _("Inbox"),
                    'action': request.env.ref('hr_official_docs.documents_unread_list').id,
                    'icon': "fa fa-inbox",
                    #'badge': request.env['hr_official_docs.document'].search([['stage_id.operator', '=', request.env.uid],['stage_id.iscompleted','=',False]], count=True) or 0,
                    'badge': request.env['hr_official_docs.document'].search([['stage_id.operator', '=', request.env.uid]], count=True) or 0,
                }, {
                    'id': 'editor',
                    'tooltip': _("Editor"),
                    'action': request.env.ref('hr_official_docs.documents_editor_list').id,
                    'icon': "fa fa-pencil",
                    'badge': request.env['hr_official_docs.document'].search([['create_uid', '=', request.env.uid],['stage_id.prev_stage_id','=',False]], count=True) or 0,
                }, {
                    'id': 'partner',
                    'tooltip': _("Partner"),
                    'action': request.env.ref('hr_official_docs.documents_partner_list').id,
                    'icon': "fa fa-user",
                    'badge': request.env['hr_official_docs.document'].search([['partner_id', '=', request.env.uid],['stage_id.next_stage_id','!=',False]], count=True) or 0,
                }
            ]
            }
        else:
            actions={
                'actions': [
                    {
                        'id': 'inbox',
                        'tooltip': _("Inbox"),
                        'action': request.env.ref('hr_official_docs.documents_unread_list').id,
                        'icon': "fa fa-inbox",
                        #'badge': request.env['hr_official_docs.document'].search([['stage_id.operator', '=', request.env.uid],['stage_id.iscompleted','=',False]], count=True) or 0,
                        'badge': request.env['hr_official_docs.document'].sudo().search([['stage_id.operator', '=', request.env.uid]], count=True) or 0,
                    }, {
                        'id': 'editor',
                        'tooltip': _("Editor"),
                        'action': request.env.ref('hr_official_docs.documents_editor_list').id,
                        'icon': "fa fa-pencil",
                        'badge': request.env['hr_official_docs.document'].search([['create_uid', '=', request.env.uid],['stage_id.prev_stage_id','=',False]], count=True) or 0,
                    }, {
                        'id': 'partner',
                        'tooltip': _("Partner"),
                        'action': request.env.ref('hr_official_docs.documents_partner_list').id,
                        'icon': "fa fa-user",
                        'badge': request.env['hr_official_docs.document'].sudo().search([['partner_id', '=', request.env.uid],['stage_id.next_stage_id','!=',False]], count=True) or 0,
                    }
                ]
            }
        return actions