# -*- coding: utf-8 -*-
###################################################################################
#   
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################

from odoo import api, fields, models

class OfficDocumentsSettings(models.TransientModel):
    
    _inherit = "res.config.settings"
    
    enable_workflow_prev_stage = fields.Boolean(
        "Allow jumping back in stages",
        help="Allow managers to jump back in stages."
    )

    @api.model
    def get_values(self):
        res = super(OfficDocumentsSettings, self).get_values()
        get_param = self.env["ir.config_parameter"].sudo().get_param
        
        res.update(
            enable_workflow_prev_stage=get_param("hr_official_docs.enable_workflow_prev_stage"),
        )
        return res
        
    def set_values(self):
        super(OfficDocumentsSettings, self).set_values()
        
        config = self.env["ir.config_parameter"]
        set_param = config.sudo().set_param
        
        set_param("hr_official_docs.enable_workflow_prev_stage", self.enable_workflow_prev_stage)
        
        