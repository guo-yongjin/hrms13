# -*- coding: utf-8 -*-
###################################################################################
#   
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################

import logging

from odoo import models, api, fields

_logger = logging.getLogger(__name__)

class Stage(models.Model):
    
    _name = "hr_official_docs.stage"
    _inherit = ['mail.thread']
    _description = "Offic Stage"
    _order = "sequence"
    _sql_constraints = [
        ('sequence_unique', 'unique(sequence)', 'Please enter a unique sequence.'),
    ]
    
    #===========================================================================
    # Variables
    #===========================================================================
    
    name = fields.Char(
        required=True,
        translate=True
    )
    
    document_ids = fields.One2many(
        "hr_official_docs.document",
        "stage_id",
    )
    
    sequence = fields.Integer(
        required=True,
        copy=False
    )
    
    '''next_stage_id = fields.Many2one(
        "hr_official_docs.stage",
        compute="_compute_next_stage_id"
    )
    
    prev_stage_id = fields.Many2one(
        "hr_official_docs.stage",
        compute="_compute_prev_stage_id"
    )'''
    
    #next_stage_group = fields.Many2one('hr_official_docs.groups')
    #prev_stage_group = fields.Many2one('hr_official_docs.groups')
    
    #has_read_access_for_users = fields.Boolean()
    #has_read_access_for_directors = fields.Boolean()
    
    #has_write_access_for_directors = fields.Boolean()
    #has_write_access_for_managers = fields.Boolean()

        #===========================================================================
    # Global Helper Functions
    #===========================================================================
    
    @api.model
    def get_first_stage(self):
        records = self.search([])
        if records:
            return records[0]
        else:
            return False
