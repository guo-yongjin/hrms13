# -*- coding: utf-8 -*-
###################################################################################
#   
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################

import logging

from odoo import _
from odoo import models, fields

_logger = logging.getLogger(__name__)

class Template(models.Model):
    
    _name = "hr_official_docs.template"
    _description = "Offic Template"
    _inherit = ["mail.thread", "mail.activity.mixin"]
    
    
    #===========================================================================
    # Variables
    #===========================================================================
    
    name = fields.Char(
        required=True,
        track_visibility="onchange",
        translate=True
    )
    
    document_ids = fields.One2many(
        "hr_official_docs.document",
        "template_id",
        "Documents"
    )
    
    document_name = fields.Char(
        track_visibility="onchange",
        translate=True
    )
    
    
    document_description = fields.Html(
        translate=True
    )
    
