# -*- coding: utf-8 -*-
###################################################################################
#   
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################

import logging

from odoo import _
from odoo import models, fields,api

_logger = logging.getLogger(__name__)

class UserTemplate(models.Model):
    
    _name = "hr_official_docs.usertemplate"
    _description = "Offic Template"
    _inherit = ["mail.thread", "mail.activity.mixin"]
    
    #_access_groups_fields = None
    #_access_groups_strict = False
    #_access_groups_mode = True
    
    #===========================================================================
    # Variables
    #===========================================================================
    
    name = fields.Char(
        required=True,
        track_visibility="onchange",
        translate=True
    )
    
    template_id = fields.Many2one(
        "hr_official_docs.template",
        "Template"
    )
    
    document_ids = fields.One2many(
        "hr_official_docs.document",
        "template_id",
        "Documents"
    )

    stage_ids = fields.One2many(
        "hr_official_docs.userstage",
        "template_id",
        "Stages"
    )
    
    document_name = fields.Char(
        track_visibility="onchange",
        translate=True
    )
    
    document_description = fields.Html(
        compute ='_compute_description', inverse='_set_description',store=True
    )
    
    #===========================================================================
    # Helper Functions
    #===========================================================================
    
    def _get_document_context(self):
        context = {
            "form_view_initial_mode": "edit",
            "force_detailed_view": True,
            "default_template_id": self.id,
        }
        
        if self.document_name:
            context.update({"default_name": self.document_name})
        
        if self.document_description:
            context.update({"default_description": self.document_description})
            
        return context
    
    #===========================================================================
    # View Actions
    #===========================================================================
        
    def action_create_document(self):
        context = self._get_document_context()
        
        return {
            "name": _("Create New Document"),
            "type": "ir.actions.act_window",
            "view_mode": "form",
            "res_model": "hr_official_docs.document",
            "target": "current",
            "context": context,
        }

    #===========================================================================
    # View Actions
    #===========================================================================
    def action_create_stages(self):
        #context = self._get_stage_context()
        stages= self.env['hr_official_docs.stage'].search([],order='sequence  asc',)
        stage_id = None
        prev_id = None
        i=1
        for re in stages:
            stage_id = self.env['hr_official_docs.userstage'].create({
                'name': re.name,
                'template_id': self.id,
                'stage_id': re.id,
                'sequence': re.sequence,
            })

        return {
            "name": _("User Template"),
            "type": "ir.actions.act_window",
            "view_mode": "form",
            'res_id':self.id,
            "res_model": "hr_official_docs.usertemplate",
            "target": "current",
            "context": self.env.context,
        }
    
    @api.onchange('template_id')
    def _compute_description(self):
        self.document_description = self.template_id.document_description
    
    def _set_description(self):
        for res in self:
            res.document_description = self.document_description
