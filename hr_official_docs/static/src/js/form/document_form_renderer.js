/**********************************************************************************
* 
*    Copyright (C) 2019 RedTN 
* 
*  此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
*  这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
*
*  您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
**********************************************************************************/

odoo.define('hr_official_docs.DocumentFormRenderer', function (require) {
"use strict";

var core = require('web.core');
var ajax = require('web.ajax');
var config = require('web.config');
var session = require('web.session');

var FormRenderer = require('web.FormRenderer');

var _t = core._t;
var QWeb = core.qweb;

var DocumentFormRenderer = FormRenderer.extend({
	_renderHeaderButton: function (node) {
		if(node.attrs.name === "set_stage_to_next" && this.state.data.next_stage_name) {
			node.attrs.string = _t("Forward to ") + this.state.data.next_stage_name;
		}
		if(node.attrs.name === "set_stage_to_prev" && this.state.data.prev_stage_name) {
			node.attrs.string = _t("Backward to ") + this.state.data.prev_stage_name;
		}
		return this._super(node);
	},
});

return DocumentFormRenderer;

});
