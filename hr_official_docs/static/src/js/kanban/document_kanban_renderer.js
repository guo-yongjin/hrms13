/**********************************************************************************
* 
*    Copyright (C) 2019 RedTN 
* 
*  此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
*  这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
*
*  您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
*
**********************************************************************************/

odoo.define('hr_official_docs.DocumentKanbanRenderer', function (require) {
"use strict";

var core = require('web.core');
var ajax = require('web.ajax');
var config = require('web.config');
var session = require('web.session');

var KanbanRenderer = require('web.KanbanRenderer');

var _t = core._t;
var QWeb = core.qweb;

var DocumentKanbanRenderer = KanbanRenderer.extend({
	custom_events: _.extend({}, KanbanRenderer.prototype.custom_events, {
		button_clicked: '_buttonClicked',
    }),
	init: function (parent, state, params) {
		this._super.apply(this, arguments);
		this.kanban_sidebar = {};
	},
	willStart: function () {
		var load_sidebar = this._load_kanban_sidebar_data();
        return $.when(this._super.apply(this, arguments), load_sidebar);
    },
    _renderView: function () {
    	this._renderSidebar();
        return this._super.apply(this, arguments);
    },
    _renderSidebar: function () {
    	this.$el.parent().find('.mk_quality_docs_document_sidebar').remove();
    	if(!config.device.isMobile) {
    		this.sidebar = $("<div>", {
	    		class: "mk_quality_docs_document_sidebar",
	    		html: $(QWeb.render('hr_official_docs.KanbanDocumentSidebar', {
	                widget: this,
	                data: this.kanban_sidebar,
	            })),
	    	});
    		this.sidebar.find('.mk_quality_docs_sidebar_actions a').on(
    				'click', _.bind(this._on_sidebar_action, this));
    		this.$el.before(this.sidebar);    	
    		var context = this.state.getContext();
	    	if (context.sidebar_action) {
	    		this.sidebar.find("#" + context.sidebar_action).addClass('active');
	    	}
    	}
    },
    _load_kanban_sidebar_data: function() {
    	var self = this;
    	/*return $.when(ajax.jsonRpc('/sidebar/hr_official_docs_document/kanban')).done(function(result) {
        	self.kanban_sidebar = _.extend({}, self.kanban_sidebar, result);
        });*/
    	return this._rpc({
            route: '/sidebar/hr_official_docs_document/kanban',
        }).then(function(result) {
        	self.kanban_sidebar = _.extend({}, self.kanban_sidebar, result);
        });
    },
    _on_sidebar_action: function(ev) {
    	this.trigger_up('sidebar_action', {
    		action: $(ev.currentTarget).data('action'),
    	});
    	ev.stopPropagation();
    	ev.preventDefault();
    },
    _buttonClicked: function(ev) {
    	var index = _.findIndex(this.kanban_sidebar.actions, function(action) {
			return action.id === "inbox";
		});
    	this._renderSidebar();
    },
});

return DocumentKanbanRenderer;

});
