/**********************************************************************************
* 
*    Copyright (C) 2019 RedTN 
* 
*  此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
*  这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
*
*  您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
*
**********************************************************************************/

odoo.define('hr_official_docs.DocumentKanbanView', function (require) {
"use strict";

var core = require('web.core');
var registry = require('web.view_registry');

var KanbanView = require('web.KanbanView');

var FileKanbanRenderer = require('hr_official_docs.DocumentKanbanRenderer');
var FileKanbanController = require('hr_official_docs.DocumentKanbanController');

var _t = core._t;
var QWeb = core.qweb;

var DocumentKanbanView = KanbanView.extend({
	config: _.extend({}, KanbanView.prototype.config, {
		Renderer: FileKanbanRenderer,
        Controller: FileKanbanController,
    }),
});

registry.add('offic_document_kanban', DocumentKanbanView);

return DocumentKanbanView;

});
