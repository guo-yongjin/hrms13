# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2019 RedTN 
#
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################


{
    'name': '加班统计',
    'summary': '加班统计',
    'version':'13.0.1',
    'license':'AGPL-3',
    'description': """
    管理加班，包括申请、批准、统计等。
""",
    'category': 'Human Resources',
    'author' : 'RedTN',
    'website': 'http://www.redtn.cn',
    'depends':['hr_contract','hr_attendance','hr'],
    'data': [
        'security/ir.model.access.csv',
        'security/rule.xml',
        'views/hr_overtime_view.xml',
        'data/hr_overtime_data.xml'
    ],
    'images': ['static/description/banner.jpg'],
    'installable': True,
    'auto_install': False,
    'application': False,
}



# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2:
