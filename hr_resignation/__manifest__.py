# -*- coding: utf-8 -*-
###################################################################################
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################

{
    'name': 'HRMS 离职',
    'version': '13.0.2.0.0',
    'summary': '处理员工离职过程',
    'author': 'guo gongjin',
    'company': 'redtn.cn',
    'website': 'https://www.redtn.cn',
    'depends': ['mail','hr_payroll_cn'],
    'category': 'Human Resources/resignation',
    'demo': [],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'data/resign_employee.xml',
        'views/hr_employee.xml',
        'views/resignation_view.xml',
        'views/approved_resignation.xml',
        'views/resignation_sequence.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': True,
    'images': ['static/description/icon.png'],
    'license': 'AGPL-3',
}

