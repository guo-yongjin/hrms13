odoo.define('hrms_dashboard.Dashboard', function (require) {
"use strict";

var AbstractAction = require('web.AbstractAction');
var ajax = require('web.ajax');
//var ControlPanelMixin = require('web.ControlPanelMixin');
var core = require('web.core');
var rpc = require('web.rpc');
var session = require('web.session');
var web_client = require('web.web_client');

var _t = core._t;
var QWeb = core.qweb;

var HrDashboard = AbstractAction.extend({
    template: 'HrDashboardMain',
    events: {
        'click .hr_leave_request_approve': 'leaves_to_approve',
        'click .hr_leave_allocations_approve': 'leave_allocations_to_approve',
        'click .hr_timesheets': 'hr_timesheets',
        'click .hr_job_application_approve': 'job_applications_to_approve',
        'click .hr_expenses_to_approve': 'expenses_to_approve',
        'click .hr_attendance_today': 'attendance_today',
        'click .hr_payslip':'hr_payslip',
        'click .hr_contract':'hr_contract',
        'click .hr_employee':'hr_employee',
        'click .leaves_request_month':'leaves_request_month',
        'click .leaves_request_today':'leaves_request_today',
        'click .hr_all_timesheets':'action_timesheets',
        'click .hr_resignation_approve':'resignation_approve',        
        "click .o_hr_attendance_sign_in_out_icon": function() {
            this.$('.o_hr_attendance_sign_in_out_icon').attr("disabled", "disabled");
            this.update_attendance();
        },
        'click #generate_payroll_pdf': function(){this.generate_payroll_pdf("bar");},
    },

    init: function(parent, context) {
        this._super(parent, context);

        this.date_range = 'week';  // possible values : 'week', 'month', year'
        this.date_from = moment().subtract(1, 'week');
        this.date_to = moment();
        this.dashboards_templates = ['LoginEmployeeDetails', 'ManagerDashboard', 'EmployeeDashboard'];
        this.employee_birthday = [];
    //    this.upcoming_events = [];
        this.announcements = [];
    },

    willStart: function() {
        var self = this;
//        return $.when(ajax.loadLibs(this), this._super()).then(function() {
            return self.fetch_data();
//        });
    },

    start: function() {
        var self = this;
        this.set("title", _t('Dashboard '));
        return this._super().then(function() {
            self.update_cp();
            self.render_dashboards();
            self.render_graphs();
            self.$el.parent().addClass('oe_background_grey');
        });
    },

    fetch_data: function() {
        var self = this;
        var def1 =  this._rpc({
                model: 'hr.employee',
                method: 'get_user_employee_details'
        }).then(function(result) {
            self.login_employee =  result[0];
        });
        var def2 = self._rpc({
            model: "hr.employee",
            method: "get_employees",
        })
        .then(function (res) {
            self.emp_table = res;
        });
        return $.when(def1, def2);
    },

    render_dashboards: function() {
        var self = this;
        if (this.login_employee){
            _.each(this.dashboards_templates, function(template) {
                self.$('.o_hr_dashboard').append(QWeb.render(template, {widget: self}));
            });
            }
        else{
            self.$('.o_hr_dashboard').append(QWeb.render('EmployeeWarning', {widget: self}));
            }
    },

    render_graphs: function(){
        var self = this;
        if (this.login_employee){
            self.render_department_employee();
            self.render_leave_graph();
            self.render_join_resign_trends();
            self.render_monthly_attrition();
            self.render_leave_trend();
            self.render_attend_trend();
            self.graph();
        }
    },

    on_reverse_breadcrumb: function() {
        var self = this;
        web_client.do_push_state({});
        this.update_cp();
        this.fetch_data().then(function() {
            self.$('.o_hr_dashboard').empty();
            self.render_dashboards();
            self.render_graphs();
        });
    },

    update_cp: function() {
        var self = this;
//        this.update_control_panel(
//            {breadcrumbs: self.breadcrumbs}, {clear: true}
//        );
    },

    get_emp_image_url: function(employee){
        return window.location.origin + '/web/image?model=hr.employee&field=image&id='+employee;
    },

    update_attendance: function () {
        var self = this;
        this._rpc({
            model: 'hr.employee',
            method: 'attendance_manual',
            args: [[self.login_employee.id], 'hr_attendance.hr_attendance_action_my_attendances'],
        })
        .then(function(result) {
            var attendance_state =self.login_employee.attendance_state;
            var message = ''
            var action_client = {
                type: "ir.actions.client",
                name: _t('Dashboard '),
                tag: 'hr_dashboard',
            };
            self.do_action(action_client, {clear_breadcrumbs: true});
            if (attendance_state == 'checked_in'){
                message = 'Checked Out'
            }
            else if (attendance_state == 'checked_out'){
                message = 'Checked In'
            }
            self.trigger_up('show_effect', {
                message: _t("Successfully " + message),
                type: 'rainbow_man'
            });
        });

    },

    hr_payslip: function(e){
        var self = this;
        e.stopPropagation();
        e.preventDefault();
        var options = {
            on_reverse_breadcrumb: this.on_reverse_breadcrumb,
        };
        this.do_action({
            name: _t("Employee Payslips"),
            type: 'ir.actions.act_window',
            res_model: 'hr.payslip',
            view_mode: 'tree,form,calendar',
            views: [[false, 'list'],[false, 'form']],
            domain: [['employee_id','=', this.login_employee.id],['state','=',['done']]],
            target: 'current'
        }, options)
    },

    hr_contract: function(e){
        var self = this;
        e.stopPropagation();
        e.preventDefault();
        session.user_has_group('hr.group_hr_user').then(function(has_group){
            if(has_group){
                var options = {
                    on_reverse_breadcrumb: self.on_reverse_breadcrumb,
                };
                self.do_action({
                    name: _t("Contracts"),
                    type: 'ir.actions.act_window',
                    res_model: 'hr.contract',
                    view_mode: 'tree,form,calendar',
                            views: [[false, 'list'],[false, 'form']],
                    context: {
                        'search_default_employee_id': self.login_employee.id,
                    },
                    target: 'current'
                }, options)
            }
        });

    },

    leaves_request_month: function(e) {
        var self = this;
        e.stopPropagation();
        e.preventDefault();
        var options = {
            on_reverse_breadcrumb: this.on_reverse_breadcrumb,
        };
        var date = new Date();
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        var fday = firstDay.toJSON().slice(0,10).replace(/-/g,'-');
        var lday = lastDay.toJSON().slice(0,10).replace(/-/g,'-');
        this.do_action({
            name: _t("This Month Leaves"),
            type: 'ir.actions.act_window',
            res_model: 'hr.leave',
            view_mode: 'tree,form,calendar',
            views: [[false, 'list'],[false, 'form']],
            domain: [['date_from','>', fday],['state','=','validate'],['date_from','<', lday]],
            target: 'current'
        }, options)
    },

    leaves_request_today: function(e) {
        var self = this;
        var date = new Date();
        e.stopPropagation();
        e.preventDefault();
        var options = {
            on_reverse_breadcrumb: this.on_reverse_breadcrumb,
        };
        this.do_action({
            name: _t("Leaves Today"),
            type: 'ir.actions.act_window',
            res_model: 'hr.leave',
            view_mode: 'tree,form,calendar',
            views: [[false, 'list'],[false, 'form']],
            domain: [['date_from','<=', date], ['date_to', '>=', date], ['state','=','validate']],
            target: 'current'
        }, options)
    },
    leaves_to_approve: function(e) {
        var self = this;
        e.stopPropagation();
        e.preventDefault();
        var options = {
            on_reverse_breadcrumb: this.on_reverse_breadcrumb,
        };
        this.do_action({
            name: _t("Leave Request"),
            type: 'ir.actions.act_window',
            res_model: 'hr.leave',
            view_mode: 'tree,form,calendar',
            views: [[false, 'list'],[false, 'form']],
            domain: [['state','in',['confirm','validate1']]],
            target: 'current'
        }, options)
    },
    leave_allocations_to_approve: function(e) {
        var self = this;
        e.stopPropagation();
        e.preventDefault();
        var options = {
            on_reverse_breadcrumb: this.on_reverse_breadcrumb,
        };
        this.do_action({
            name: _t("Leave Allocation Request"),
            type: 'ir.actions.act_window',
            res_model: 'hr.leave.allocation',
            view_mode: 'tree,form,calendar',
            views: [[false, 'list'],[false, 'form']],
            domain: [['state','in',['confirm', 'validate1']]],
            target: 'current'
        }, options)
    },

    hr_timesheets: function(e) {
         var self = this;
        e.stopPropagation();
        e.preventDefault();
        var options = {
            on_reverse_breadcrumb: this.on_reverse_breadcrumb,
        };
        this.do_action({
            name: _t("Timesheets"),
            type: 'ir.actions.act_window',
            res_model: 'account.analytic.line',
            view_mode: 'tree,form',
            views: [[false, 'list'], [false, 'form']],
            context: {
                'search_default_month': true,
            },
            domain: [['employee_id','=', this.login_employee.id]],
            target: 'current'
        }, options)
    },
    job_applications_to_approve: function(event){
        var self = this;
        event.stopPropagation();
        event.preventDefault();
        var options = {
            on_reverse_breadcrumb: this.on_reverse_breadcrumb,
        };
        this.do_action({
            name: _t("Applications"),
            type: 'ir.actions.act_window',
            res_model: 'hr.applicant',
            view_mode: 'tree,kanban,form,pivot,graph,calendar',
            views: [[false, 'list'],[false, 'kanban'],[false, 'form'],
                    [false, 'pivot'],[false, 'graph'],[false, 'calendar']],
            context: {},
            target: 'current'
        }, options)
    },

    expenses_to_approve: function(event){
        var self = this;
        event.stopPropagation();
        event.preventDefault();
        var options = {
            on_reverse_breadcrumb: this.on_reverse_breadcrumb,
        };
/*        this.do_action({
            name: _t("Applications"),
            type: 'ir.actions.act_window',
            res_model: 'hr.expense.sheet',
            view_mode: 'tree,kanban,form,pivot,graph,calendar',
            views: [[false, 'list'],[false, 'kanban'],[false, 'form'],
                    [false, 'pivot'],[false, 'graph']],
            context: {},
            target: 'current'
        }, options)
*/
        this.do_action({
            name: _t("Expense Reports to Approve"),
            type: 'ir.actions.act_window',
            res_model: 'hr.expense.sheet',
            view_mode: 'tree,kanban,form,pivot,graph',
            views: [[false, 'list'],[false, 'form'],[false, 'kanban'],[false, 'pivot'],[false, 'graph']],
            context: {
                    'search_default_submitted': true,
                    },
            domain: [],
//            search_view_id: self.employee_data.attendance_search_view_id,
            target: 'current'
        },options)
    },
    attendance_today: function(event){
        var self = this;
        event.stopPropagation();
        event.preventDefault();
        var options = {
            on_reverse_breadcrumb: this.on_reverse_breadcrumb,
        };
        var date = new Date();
        var seperator1 = "-";
        var strDate = date.getDate();
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var currentdate = year + seperator1 + month + seperator1 + strDate;
        var sd= new Date(currentdate+" 00:00:00");
        var ed = new Date(currentdate+" 23:59:59");
 /*       this.do_action({
            name: _t("Applications"),
            type: 'ir.actions.act_window',
            res_model: 'hr.attendance',
            view_mode: 'tree,kanban,form,pivot,graph,calendar',
            views: [[false, 'list'],[false, 'kanban'],[false, 'form']],
            context: {
                        'search_default_employee_id': self.login_employee.id,
                'search_default_check_in_filter': 'today',
            },
            domain: [['check_in','<=', ed], ['check_in', '>=', sd],
            ('|',['employee_id','=',self.login_employee.id],['employee_id.parent_id','=',self.login_employee.id])],
            target: 'current'
        }, options)*/
        this.do_action({
            name: _t("Attendances"),
            type: 'ir.actions.act_window',
            res_model: 'hr.attendance',
            view_mode: 'tree,form',
            views: [[false, 'list'],[false, 'kanban'],[false, 'form']],
            context: {
                    'search_default_check_in_filter': 'today',
                    },
            domain: [],
/*            search_view_id: self.employee_data.attendance_search_view_id,*/
            target: 'current'
        },options)
    },

    action_timesheets: function(event) {
        var self = this;
        event.stopPropagation();
        event.preventDefault();
        var options = {
            on_reverse_breadcrumb: this.on_reverse_breadcrumb,
        };
        this.do_action({
            name: _t("All Timesheets"),
            type: 'ir.actions.act_window',
            res_model: 'account.analytic.line',
            view_mode: 'tree,form',
            views: [[false, 'list'],[false, 'kanban'],[false, 'form']],
            context: {
                    'search_default_week': true,
                    'search_default_groupby_employee': true,
                    'search_default_groupby_project': true,
                    'search_default_groupby_task': true,
                    },
            domain: [['project_id', '!=', false]],
//            search_view_id: self.login_employee.timesheet_view_id,
            target: 'current'
        },options)
    },
    resignation_approve: function(event) {
        var self = this;
        event.stopPropagation();
        event.preventDefault();
        var options = {
            on_reverse_breadcrumb: this.on_reverse_breadcrumb,
        };
        this.do_action({
            name: _t("Resignation Approve"),
            type: 'ir.actions.act_window',
            res_model: 'hr.resignation',
            view_mode: 'tree,form',
            views: [[false, 'list'],[false, 'form']],
            context: {
                    'search_default_week': true,
                    'search_default_groupby_employee': true,
                    'search_default_groupby_project': true,
                    'search_default_groupby_task': true,
                    },
            domain: [],
//            search_view_id: self.login_employee.timesheet_view_id,
            target: 'current'
        },options)
    },
    
    // Function which gives random color for charts.
    getRandomColor: function () {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++ ) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    },
    //render graph
    render_department_employee:function(){
        var self = this;
        rpc.query({
            model: "hr.employee",
            method: "get_dept_employee",
        }).then(function (data) {
                var dataset = []
                var labels = []
                data.forEach(function(d) {
                    labels.push(d['label'])
                    dataset.push(d['value'])
                });

            Chart.helpers.merge(Chart.defaults.global.plugins.datalabels, {
                    color: function(ctx) {
                        // use the same color as the border
                        return ctx.dataset.borderColor
                    },
						font: {
							weight: 'bold',
						},
                });
                
                //Pie Chart
                var piectx = self.$el.find('#emp_graph');
                var bg_color_list = []
                for (var i=0;i<=dataset.length;i++){
                    bg_color_list.push(self.getRandomColor())
                }

                var pieChart = new Chart(piectx, {
                    type: 'pie',
                    data: {
                        datasets: [{
                            data: dataset,
                            backgroundColor: bg_color_list,
                            //label: ''
                        }],
                        labels:labels,
                    },
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        legend: {
                                    display: true,
                                    position: 'right',
                                    labels: {
                                        fontColor: 'rgb(255, 99, 132)'
                                    }
                                }
                    },
                }); 


        });

    },
    render_join_resign_trends: function(){
        var self = this;
        rpc.query({
            model: "hr.employee",
            method: "join_resign_trends",
        }).then(function (data) {
                
            var join_dataset = []
            var labels = []
            data[0].values.forEach(function(d) {
                labels.push(d['l_month'])
                join_dataset.push(d['count'])
            });
            var resign_dataset = []
            data[1].values.forEach(function(d) {
                resign_dataset.push(d['count'])
            });


            var ctx = self.$el.find('#join_resign_trend')
                // Fills the canvas with white background
            Chart.plugins.register({
                beforeDraw: function(chartInstance) {
                var ctx = chartInstance.chart.ctx;
                ctx.fillStyle = "white";
                ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
                }
            });
            var bg_color_list = []
            for (var i=0;i<=1;i++){
                 bg_color_list.push(self.getRandomColor())
            }

		var config = {
			type: 'line',
			data: {
				labels: labels,
				datasets: [{
					label: data[0].name,
					data: join_dataset,
					borderColor: bg_color_list[0],
					backgroundColor: bg_color_list[0],
					fill: false,
					lineTension: 0,
                    pointStyle:'rect',
                    pointRadius: 5,
				}, {
					label: data[1].name,
					data: resign_dataset,
					borderColor: bg_color_list[1],
					backgroundColor: bg_color_list[1],
                    pointStyle:'triangle',
                    pointRadius: 5,
					fill: false,
					lineTension: 0,
				},]
			},
			options: {
				responsive: true,
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true
						}
					}],
					yAxes: [{
						display: true,
						ticks: {
							suggestedMin: 0,
							suggestedMax: 10,
						}
					}]
				}
			}
		};

                
                
        var myChart = new Chart(ctx, config );

        });
    },

    render_monthly_attrition: function(){
        var self = this;
        rpc.query({
            model: "hr.employee",
            method: "get_attrition_rate",
        }).then(function (data) {

            var datasets = []
            var labels = []
            data.forEach(function(d) {
                labels.push(d['month'])
                datasets.push(d['attrition_rate'])
            });
            
            var ctx = self.$el.find('#attrition_rate')
                // Fills the canvas with white background
            Chart.plugins.register({
                beforeDraw: function(chartInstance) {
                var ctx = chartInstance.chart.ctx;
                ctx.fillStyle = "white";
                ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
                }
            });
            var bg_color_list = []
            for (var i=0;i<=2;i++){
                 bg_color_list.push(self.getRandomColor())
            }


		var config = {
			type: 'line',
			data: {
				labels: labels,
				datasets: [{
					//label: _t('attrition rate'),
					data: datasets,
					borderColor: bg_color_list[0],
					backgroundColor: bg_color_list[0],
					fill: false,
					lineTension: 0,
				},]
			},
			options: {
				responsive: true,
				//tooltips: {
					//mode: 'index'
				//},
                legend: {
                            display: false,
                            position: 'right',
                        },
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true
						}
					}],
					yAxes: [{
						display: true,
						ticks: {
							suggestedMin: 0,
							suggestedMax: 10,
						}
					}]
				}
			}
		};

                
                
        var myChart = new Chart(ctx, config );
        });

    },

    render_leave_trend: function(){
        var self = this;
        rpc.query({
            model: "hr.employee",
            method: "employee_leave_trend",
        }).then(function (data) {

            var datasets = []
            var labels = []
            data.forEach(function(d) {
                labels.push(d['l_month'])
                datasets.push(d['leave'])
            });
            
            var ctx = self.$el.find('#leave_trend')
                // Fills the canvas with white background
            Chart.plugins.register({
                beforeDraw: function(chartInstance) {
                var ctx = chartInstance.chart.ctx;
                ctx.fillStyle = "white";
                ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
                }
            });
            var bg_color_list = []
            for (var i=0;i<=2;i++){
                 bg_color_list.push(self.getRandomColor())
            }


		var config = {
			type: 'line',
			data: {
				labels: labels,
				datasets: [{
					//label: _t('leave'),
					data: datasets,
					borderColor: bg_color_list[0],
					backgroundColor: bg_color_list[0],
					fill: false,
					lineTension: 0,
				},]
			},
			options: {
				responsive: true,
                legend: {
                            display: false,
                            position: 'right',
                        },
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true
						}
					}],
					yAxes: [{
						display: true,
						ticks: {
							suggestedMin: 0,
							suggestedMax: 10,
						}
					}]
				}
			}
		};

                
                
        var myChart = new Chart(ctx, config );
 
        });
    },

    render_attend_trend: function(){
        var self = this;
        rpc.query({
            model: "hr.employee",
            method: "get_attend_trends",
        }).then(function (data) {

            var attend_datasets = []
            data[0].values.forEach(function(d) {
                attend_datasets.push(d['count'])
            });
            var over_datasets = []
            var labels = []
            data[1].values.forEach(function(d) {
                labels.push(d['l_month'])
                over_datasets.push(d['count'])
            });
           
            var ctx = self.$el.find('#attend_trend')
                // Fills the canvas with white background
            Chart.plugins.register({
                beforeDraw: function(chartInstance) {
                var ctx = chartInstance.chart.ctx;
                ctx.fillStyle = "white";
                ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
                }
            });
            var bg_color_list = []
            for (var i=0;i<=2;i++){
                 bg_color_list.push(self.getRandomColor())
            }


		var config = {
			type: 'line',
			data: {
				labels: labels,
				datasets: [{
					label: _t('attend'),
					data: attend_datasets,
					borderColor: bg_color_list[0],
					backgroundColor: bg_color_list[0],
					fill: false,
					lineTension: 0,
				},{
					label: _t('overtime'),
					data: over_datasets,
					borderColor: bg_color_list[1],
					backgroundColor: bg_color_list[1],
					fill: false,
					lineTension: 0,
				},]
			},
			options: {
				responsive: true,
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true
						}
					}],
					yAxes: [{
						display: true,
						ticks: {
							suggestedMin: 0,
							suggestedMax: 10,
						}
					}]
				}
			}
		};

                
                
        var myChart = new Chart(ctx, config );
 
        });
    },

    render_leave_graph:function(){
        var self = this;
        rpc.query({
                model: "hr.employee",
                method: "get_department_leave",
            }).then(function (data) {
                var months = data[0];
                var leave_mon = data[1];
                var depts = data[2]
                var leave_dep = data[3]

                
                //bar Chart
                var barctx = self.$el.find('#mon_leave');
                var bg_color_list = []
                for (var i=0;i<=leave_mon.length;i++){
                    bg_color_list.push(self.getRandomColor())
                }
                var bg_color_list1 = []
                for (var i=0;i<=leave_dep.length;i++){
                    bg_color_list1.push(self.getRandomColor())
                }

                var barChart = new Chart(barctx, {
                    type: 'bar',
                    data: {
                        datasets: [{
                            data: leave_mon,
                            backgroundColor: bg_color_list,
                            //label: _t('month leaves')
                        },],
                        labels:months,
                    },
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        legend: {
                                    display: false,
                                    position: 'top',
                                }
                    },
                }); 

                 //Pie Chart
                var piectx = self.$el.find('#dep_leave');

                var pieChart = new Chart(piectx, {
                    type: 'pie',
                    data: {
                        datasets: [{
                            data: leave_dep,
                            backgroundColor: bg_color_list1,
                            //label: ''
                        }],
                        labels:depts,
                    },
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        legend: {
                                    display: true,
                                    position: 'right',
                                    labels: {
                                        fontColor: 'rgb(255, 99, 132)'
                                    }
                                }
                    },
                }); 

     
                
        });
    },

    // Here we are plotting bar,pie chart
    graph: function() {
        var self = this;
        rpc.query({
                model: "hr.employee",
                method: "get_payslip",
            }).then(function (data) {
                var payroll_dataset = []
                var payroll_label = []
                data[0].values.forEach(function(d) {
                    payroll_label.push(d['l_month'])
                    payroll_dataset.push(d['count'])
                });
                var ctx = self.$el.find('#payroll')
                // Fills the canvas with white background
                Chart.plugins.register({
                  beforeDraw: function(chartInstance) {
                    var ctx = chartInstance.chart.ctx;
                    ctx.fillStyle = "white";
                    ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
                  }
                });
                var bg_color_list = []
                for (var i=0;i<=12;i++){
                    bg_color_list.push(self.getRandomColor())
                }
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: payroll_label,
                        datasets: [{
                            label: data[0].name,
                            data: payroll_dataset,
                            backgroundColor: bg_color_list,
                            borderColor: bg_color_list,
                            borderWidth: 1,
                            pointBorderColor: 'white',
                            pointBackgroundColor: 'red',
                            pointRadius: 5,
                            pointHoverRadius: 10,
                            pointHitRadius: 30,
                            pointBorderWidth: 2,
                            pointStyle: 'rectRounded'
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    min: Math.round(Math.max.apply(null,payroll_dataset)/5),
                                    max: Math.max.apply(null,payroll_dataset),
                                    //min: 1000,
                                    //max: 100000,
                                    stepSize: (Math.max.apply(null,payroll_dataset)-Math.min.apply(null,payroll_dataset))/5
                                //    payroll_dataset.reduce((pv,cv)=>{return pv + (parseFloat(cv)||0)},0)
                                //    /payroll_dataset.length,
                                 }
                            }]
                        },
                        responsive: true,
                        maintainAspectRatio: true,
                        animation: {
                            duration: 100, // general animation time
                        },
                        hover: {
                            animationDuration: 500, // duration of animations when hovering an item
                        },
                        responsiveAnimationDuration: 500, // animation duration after a resize
                        legend: {
                            display: false,
                            labels: {
                                fontColor: 'black',
                            }
                        },
                    },
                });
               
                
        });
        

    },
    
    generate_payroll_pdf: function(chart) {
        if (chart == 'bar'){
            var canvas = document.querySelector('#payroll');
        }
        else if (chart == 'pie') {
            var canvas = document.querySelector('#attend_trend');
        }

        //creates image
        var canvasImg = canvas.toDataURL("image/jpeg", 1.0);
        var doc = new jsPDF('landscape');
        doc.setFontSize(20);
        doc.addImage(canvasImg, 'JPEG', 10, 10, 280, 150 );
        doc.save('report.pdf');
    },
    

});


core.action_registry.add('hr_dashboard', HrDashboard);

return HrDashboard;

});
