# -*- coding: utf-8 -*-
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################


{
    'name': '中国企业会计科目',
    'version': '13.0.12.14',
    'author': 'guo yongjin',
    'category': 'Localization',
    'website': 'http://www.redtn.cn',
    'license': 'LGPL-3',
    'sequence': 12,
    'summary': """    
    多级科目. 中国会计科目.增加科目分组。中国税率。
    """,
    'description': """
    最新中国化财务，主要针对标准会计科目表作了优化。
      redtn.cn
 
    """,
    'depends': [
        'account',
        'l10n_cn',
        'l10n_cn_small_business',
        'hr',
    ],
    'images': ['static/description/banner.png'],
    'data': [
        'views/res_partner_category_views.xml',
        'views/res_partner_views.xml',
        'views/res_currency_views.xml',
        'views/account_account_views.xml',
        'views/account_views.xml',
        'views/product.xml',
        'views/assets.xml',
        'data/ir_default_data.xml',
        'data/base_data.xml',
        'data/res_country_data.xml',
        'data/res_currency_data.xml',
        'data/account_chart_data.xml',
        'data/account_account_tag_data.xml',
        'data/account.group.csv',
        'data/account.account.template.csv',
        'data/account_tax_template_data.xml',
        'data/account_chart_template_data.xml',
        'data/hr_data.xml',
    ],
    'pre_init_hook': 'pre_init_hook',
    'post_init_hook': 'post_init_hook',
    'installable': True,
    'application': False,
    'auto_install': True,
}
