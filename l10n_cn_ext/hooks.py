# -*- coding: utf-8 -*-

###################################################################################
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################

from odoo import api, SUPERUSER_ID

def pre_init_hook(cr):
    """
    数据初始化，只在安装时执行，更新时不执行
    """
    cr.execute("delete from ir_module_module where to_buy = 'TRUE';")
    cr.execute("update res_partner set lang = 'zh_CN',tz ='Asia/Shanghai' where name in ('Administrator','Public user','Default User Template','Portal User Template');")
    pass

def post_init_hook(cr, registry):
    """
    数据初始化，只在安装后执行，更新时不执行
    此处不执行，只是记录，该数据已处理完成
    """
    cr.execute("UPDATE account_account_template set group_id = "
               "(select id from account_group where account_group.code_prefix=trim(substring(account_account_template.code from 1 for 1)) limit 1);")

    cr.execute("UPDATE account_account set group_id = "
               "(select id from account_group where account_group.code_prefix=trim(substring(account_account.code from 1 for 1)) limit 1);")
    cr.execute("delete from res_lang where res_lang.code not in ('zh_CN','en_US','zh_TW','zh_HK','ja_JP','ko_KR');")
    cr.execute("UPDATE res_lang  set active = 'TRUE' where code = 'zh_CN' ;")
    cr.execute("update resource_resource set tz ='Asia/Shanghai' where name in ('人力经理','人资主管','文员','项目经理','项目部总监','财务部总监','Administrator');")
    cr.execute("update res_partner set lang = 'zh_CN',tz ='Asia/Shanghai' where name in ('人力经理','人资主管','文员','项目经理','项目部总监','财务部总监','我的公司','超管');")
    pass

