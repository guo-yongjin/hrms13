# -*- coding: utf-8 -*-
###################################################################################
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################


from odoo import api, fields, models, _
from odoo.exceptions import UserError

import logging
_logger = logging.getLogger(__name__)



class AccountChartTemplate(models.Model):
    _inherit = "account.chart.template"

    @api.model
    def _prepare_transfer_account_template(self):
        ''' Prepare values to create the transfer account that is an intermediary account used when moving money
        from a liquidity account to another.

        :return:    A dictionary of values to create a new account.account.
        '''
        # 分隔符，金蝶为 "."，用友为""，注意odoo中一级科目，现金默认定义是4位头，银行是6位头
        delimiter = '.'
        digits = self.code_digits
        prefix = self.transfer_account_code_prefix or ''
        # Flatten the hierarchy of chart templates.
        chart_template = self
        chart_templates = self
        while chart_template.parent_id:
            chart_templates += chart_template.parent_id
            chart_template = chart_template.parent_id
        new_code = ''
        for num in range(1, 100):
            new_code = str(prefix.ljust(digits - 1, '0')) + delimiter + '%02d' % (num)
            rec = self.env['account.account.template'].search(
                [('code', '=', new_code), ('chart_template_id', 'in', chart_templates.ids)], limit=1)
            if not rec:
                break
        else:
            raise UserError(_('Cannot generate an unused account code.'))
        current_assets_type = self.env.ref('account.data_account_type_current_assets', raise_if_not_found=False)
        return {
            'name': _('Liquidity Transfer'),
            'code': new_code,
            'user_type_id': current_assets_type and current_assets_type.id or False,
            'reconcile': True,
            'chart_template_id': self.id,
        }

    @api.model
    def load_for_current_company(self, sale_tax_rate, purchase_tax_rate):
#        self.ensure_one()
        res = super(AccountChartTemplate, self).load_for_current_company(sale_tax_rate, purchase_tax_rate)
        # 更新父级
        company = self.env.user.company_id
        acc_ids = self.env['account.account'].sudo().search([('company_id', '=', company.id)])
        for acc in acc_ids:
            code = acc.code
            parent_account = self.env['account.account.template'].sudo().search([
                ('code', '=', code),
                ('chart_template_id', '=', self.id),
                ('parent_id', '!=', False)
            ], limit=1)
            if (len(parent_account) and int(parent_account.parent_id)>0 ) or code == '2221.01.01':
                parent_code = parent_account.parent_id.code
                if parent_code:
                    parent = self.env['account.account'].sudo().search([
                        ('company_id', '=', company.id),
                        ('code', '=', parent_code),
                    ], limit=1)
                    if len(parent):
                        _logger.info('parent_id:%s',parent.id)
                        acc.update({
                            'parent_id': parent.id,
                        })   
        return res


