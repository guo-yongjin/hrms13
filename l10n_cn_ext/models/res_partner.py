# -*- coding: utf-8 -*-

from odoo import api, models, fields, _
from odoo.exceptions import UserError, ValidationError


class ResPartner(models.Model):
    _inherit = 'res.partner'

    short_name = fields.Char('Short Name')  # 简称


class PartnerCategory(models.Model):
    _inherit = 'res.partner.category'
    _order = 'sequence, name'

    sequence = fields.Integer('Sequence', help="Used to order partner category")

class CustomEmployee(models.Model):
    _inherit = "hr.employee"
    _description = "Employee"

    tz = fields.Selection(
        string='Timezone', related='resource_id.tz', readonly=False,
        default=lambda self: self._context.get('tz') or self.env.user.tz or 'UTC',
        help="This field is used in order to define in which timezone the resources will work.")
 
