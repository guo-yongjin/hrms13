odoo.define('l10n_cn_ext.forumview', function (require) {
"use strict";

var core = require('web.core');
var view_dialogs = require('web.view_dialogs');
var KanbanColumn = require('web.KanbanColumn');
var _t = core._t;

KanbanColumn.include({
    _onEditColumn: function (event) {
        event.preventDefault();
        var context = this.data.getContext();
        new view_dialogs.FormViewDialog(this, {
            res_model: this.relation,
            res_id: this.id,
            context: context,
            title: _t("Edit Column"),
            on_saved: this.trigger_up.bind(this, 'reload'),
        }).open();
    },
});

});
