# -*- coding: utf-8 -*-
###################################################################################
#   
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################

from odoo import api, fields, models, _

class project_task(models.Model):
    _inherit = 'project.task'

    
    def tasks_issue_count(self):
        issue_lst = []
        ir_model_data = self.env['ir.model.data']
        #search_view_id = ir_model_data.get_object_reference('project', 'view_issue_search_form')[1]
        search_view_id = self.env.ref('project_create_issue.view_issue_search_form').id
        search_issue_id = self.env['project.issue'].search([('issue_id', '=',self.id)])
        for i in search_issue_id:
            issue_lst.append(i.id)
        self[0].task_issue_count = len(issue_lst)
        return{
            'name':'Issues',
            'res_model':'project.issue',
            'type':'ir.actions.act_window',
            'view_type':'form',
            'view_mode':'list',
            'view_id':self.env.ref('project_create_issue.view_issue_inherit_tree').id,
            'domain': [('issue_id', '=',self[0].id )],
            'search_view_id':search_view_id,
         }


    task_issue_count = fields.Integer(compute=tasks_issue_count,string="Task Count")


    def do_action(self):
        ir_model_data = self.env['ir.model.data']
        #search_view_id = ir_model_data.get_object_reference('project', 'view_issue_search_form')[1]
        search_view_id = self.env.ref('project_create_issue.view_issue_search_form').id
        search_issue_id = self.env['project.issue'].search([('issue_id', '=',self.id)])
        return{
            'name':'Issues',
            'res_model':'project.issue',
            'type':'ir.actions.act_window',
            'view_type':'form',
            'view_mode':'form',
            'view_id':self.env.ref('project_create_issue.view_project_issue_inherit_form').id,
            'context': {'default_user_id': self.env.user.id,'default_issue_id': self[0].id},
            #'domain': [('issue_id', '=',res[0].id )],
            'search_view_id':search_view_id,
         }

class project_issue(models.Model):
    _inherit = 'project.task'
    _name = 'project.issue'

 
    issue_id=fields.Many2one('project.task',string="Task") #是否 指向task

