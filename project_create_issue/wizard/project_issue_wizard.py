# -*- coding: utf-8 -*-
###################################################################################
#   
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################

from odoo import api, fields, models

class project_issue_wizard(models.TransientModel):
    _name = 'project.issue.wizard'

    name = fields.Char(string="Name",required=True)
    
    @api.model
    def default_get(self,fields):
        res = super(project_issue_wizard, self).default_get(fields)
        if self._context.get('active_id'):
            project_task_id = self.env['project.task'].browse(self._context.get('active_id'))
            res.update({'name':project_task_id.name})
        else:
            pass
        return res

    
    def create_issue(self):
        if self._context.get('active_id'):
            project_task_id = self.env['project.task'].browse(self._context.get('active_id'))
            vals = {
                        'user_id': project_task_id.user_id.id or False,
                        'task_id': self._context.get('active_id'),
                        'partner_id': project_task_id.partner_id.id or False,
                        'email_from': project_task_id.partner_id.email,
                        'project_id': project_task_id.project_id.id or False,
                        'name': self.name,
                        'issue_id':project_task_id.id,

                    }
            if vals:
                project_issue_id = self.env['project.task'].create(vals)
        else:
            pass
        return {
                    'name':'project.issue.form.view',
                    'res_model':'project.task',
                    'view_type':'form',
                    'view_mode':'form',
                    'res_id': project_issue_id.id,
                    'target':'current',
                    'type':'ir.actions.act_window'
                    }


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
