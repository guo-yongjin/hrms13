# -*- coding: utf-8 -*-
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################

{
    'name': '项目自定义',
    'version': '13.0.0.0',
    'author': 'guo yongjin',
    "category": "Project",
    "description": """
        此模块帮助自定义项目类别应用程序、项目、检查列表、记分卡、时间线等.
    """,
    'summary': '自定义项目类别应用程序',
    'website': 'www.redtn.cn',
    'depends':['hr','sale_timesheet','hr_expense','project','project_detail_report','hr_timesheet',
            'task_check_list','task_score_card','project_employee_task','project_create_issue',
            #'project_timeline',
            ],
    'data':[
        'views/app_theme_config_settings_view.xml',
        'views/ui_views.xml',
#        'views/template_view_views.xml',
        'security/ir.model.access.csv',
        'data/resource_data.xml',
        'data/product_data.xml',
        'data/hr_expense_data.xml',
        'data/mail_template_data.xml',
        ],
    'installable': True,
    'auto_install': False,
    'application': False,
    "images":['static/description/icon.png'],
}

