from odoo import models, fields, api, _


class CustomUsersInherit(models.Model):
    _inherit = 'hr.employee'


    
    def create_user(self):
        user_id = self.env['res.users'].create({'name': self.name,'login': self.work_email})
#        self.user_id = user_id.id
        self.address_home_id = user_id.partner_id.id
        self.user_check_tick = True
        self.env.cr.execute("update hr_employee set user_id = %s where name = %s",(user_id.id, self.name))