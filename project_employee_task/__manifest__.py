# -*- coding: utf-8 -*-
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################

{
    'name': '员工任务',
    'version': '13.0.0.0',
    'author': 'guo yongjin',
    'category': "Human Resources",
    'description': """
        此模块有助于向员工显示已分配的任务、员工任务、员工上的可见任务。员工的任务列表，
    """,
    'summary': '向员工视图显示已分配的任务',
    'website': 'www.redtn.cn',
    'depends':['base','hr','project'],
    'data':[
        'security/employee_security.xml',
        'views/employee_task.xml',
        ],
    'installable': True,
    'auto_install': False,
    "images":['static/description/Banner.png'],
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

