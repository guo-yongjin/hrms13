# -*- coding: utf-8 -*-
###################################################################################
#   
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################

from odoo import api, fields, models, _

class hr_employee(models.Model):
    _inherit = 'hr.employee'

    
    @api.depends('user_id')
    def compute_tasks_count(self):
        usr_id = 0
        ir_model_data = self.env['ir.model.data']
        search_view_id = ir_model_data.get_object_reference('project', 'view_task_search_form')[1]
        for each in self:
            if each.user_id:
                project_task_ids = self.env['project.task'].search([('user_id','=',each.user_id.id)])
                length_count = len(project_task_ids)
                each.task_count = length_count
                usr_id = each.user_id.id
            else:
                each.task_count = 0
                pass
        return{
            'name':'Employee Task',
            'res_model':'project.task',
            'type':'ir.actions.act_window',
            'view_type':'form',
            'view_mode':'list,form,kanban,calendar,pivot,graph',
            'context':{'group_by':'stage_id'},
            'domain': [('user_id', '=', usr_id)],
            'search_view_id':search_view_id,
         }

    task_count = fields.Integer(compute=compute_tasks_count,string='Task Count',readonly = True )

True