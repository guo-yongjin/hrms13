# -*- coding: utf-8 -*-

from .hooks import pre_init_hook
from .hooks import post_init_hook
from . import models
from . import controllers

from odoo import SUPERUSER_ID

MODULE = '_redtn_hrms'


def uninstall_hook(cr, registry):
    registry['ir.model.data']._module_data_uninstall(cr, SUPERUSER_ID, [MODULE])
