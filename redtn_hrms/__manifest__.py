# -*- coding: utf-8 -*-
###################################################################################
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################

{
    'name': "红橙HRMS",
    'version': '1.0.1',
    'author': 'guo yongjin',
    "website": "http://www.redtn.cn/",
    'category': 'Tools',
    "summary": "人力资源管理系统",
    'description': '''红橙HRMS，包括招聘、入职、培训、考勤、项目、销售、评估、工资等管理功能，实现人力资源全周期管理''',
    'depends': ['l10n_cn_ext','account_bank_statement_import','im_livechat','sale','hr_holidays','website','web_editor','website_forum','website_blog',
        'html_image_url_extractor','website_hr_recruitment','website_event','event_sale','web_responsive','hr_customize','hrms_dashboard','project_customize',
        #'document_customize',
        #'website','website_forum','website_blog','website_blog_excerpt_img','website_hr_recruitment','website_event',
    ],
    'data': [
        'views/base_data.xml',
        'data/config_parameter.xml',
        'data/res_groups.xml',
        'data/event_sale_data.xml',
        'views/js.xml',
        'views/webclient_templates.xml',
        'views/mail_data.xml',
        'views/ir_module_view.xml',
        'views/powered_by_view.xml',
        'security/ir.model.access.csv',
        'data/report_layout.xml',
        'data/mail_template_data.xml',
        'views/config_settings_view.xml',
        'views/ir_ui_views.xml',
        'views/qweb_view.xml',
        'views/snippets.xml',

    ],
    'qweb': [
        'static/src/xml/customize_user_menu.xml',
        'static/src/xml/debrands.xml',
    ],
    'images': ['static/description/logo.png'],
    'license': "LGPL-3",
    'pre_init_hook': 'pre_init_hook',
    'post_init_hook': 'post_init_hook',
    'auto_install': False,
    'application': True,
    'installable': True
}
