# -*- coding: utf-8 -*-

###################################################################################
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################

from odoo import api, SUPERUSER_ID

def pre_init_hook(cr):
    pass

def post_init_hook(cr, registry):
    """
    数据初始化，只在安装后执行，更新时不执行
    此处不执行，只是记录，该数据已处理完成
    """
    cr.execute("insert into ir_translation (name,res_id,lang,type,src,value,module,state) "
                "(select ir.name,menu.id,ir.lang,ir.type,ir.src,ir.value,ir.module,ir.state from ir_translation as ir right JOIN website_menu as menu on ir.src = menu.name  where ir.name ='website.menu,name' and ir.res_id != menu.id ) on conflict do nothing ")
    pass

