# -*- coding: utf-8 -*-
###################################################################################
#   
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################

from odoo import models
import json

class BlogPost(models.Model):
    _inherit = "blog.post"

    def write(self, vals):
        result = super().write(vals)
        properties = json.loads(self.cover_properties)
        image = properties.get('background-image', 'none')[4:-1]
        if image=='none' or  image=='':
            try:
                image = next(self.env['ir.fields.converter'].imgs_from_html(
                    self.content,
                    1,
                ))
            except StopIteration:
                pass  # No image
            else:
                # image = "url('"+image+"')"
                properties['background-image']= "url('"+image+"')"
                self.cover_properties = json.dumps(properties)
        return result
