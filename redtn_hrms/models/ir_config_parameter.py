# -*- coding: utf-8 -*-

from odoo import models, api, fields, tools

PARAMS = [
    'redtn_hrms.new_name',
    'redtn_hrms.new_title',
    'redtn_hrms.favicon_url',
    'redtn_hrms.planner_footer',
    'redtn_hrms.default_logo_module'
]


class IrConfigParameter(models.Model):
    _inherit = 'ir.config_parameter'

    @api.model
    @tools.ormcache()
    def get_debranding_parameters(self):
        res = {}
        for param in PARAMS:
            value = self.env['ir.config_parameter'].get_param(param)
            res[param] = value
        return res

    
    def write(self, vals, context=None):
        res = super(IrConfigParameter, self).write(vals)
        for r in self:
            if r.key in PARAMS:
                self.get_debranding_parameters.clear_cache(self)
                break
        return res

    #app_system_name = fields.Char('System Name', help=u"Setup System Name,which replace Odoo")
    group_show_author_in_apps = fields.Boolean(string="Show Author in Apps Dashboard", implied_group='redtn_hrms.group_show_author_in_apps',
                                               help=u"Uncheck to Hide Author and Website in Apps Dashboard")

    app_documentation_url = fields.Char('Documentation Url')
    app_documentation_dev_url = fields.Char('Developer Documentation Url')
    app_support_url = fields.Char('Support Url')

    # Sample Error Dialogue
    
    def error(self):
        raise ValueError

    # Sample Warning Dialogue
    
    def warning(self):
        raise Warning(_("This is a Warning"))


    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        ir_config = self.env['ir.config_parameter'].sudo()
        #app_system_name = ir_config.get_param('app_system_name', default='MyApp')


        app_documentation_url = ir_config.get_param('app_documentation_url',
                                                    default='http://www.redtn.cn/documentation/user-manual/index.htm')
        app_documentation_dev_url = ir_config.get_param('app_documentation_dev_url',
                                                        default='http://www.redtn.cn/documentation/development/index.htm')
        app_support_url = ir_config.get_param('app_support_url', default='http://www.redtn.cn/')
        res.update(
            #app_system_name=app_system_name,

            app_documentation_url=app_documentation_url,
            app_documentation_dev_url=app_documentation_dev_url,
            app_support_url=app_support_url,
        )
        return res

    
    def set_values(self):
        super(ResConfigSettings, self).set_values()
        ir_config = self.env['ir.config_parameter'].sudo()
        #ir_config.set_param("app_system_name", self.app_system_name or "")

        ir_config.set_param("app_documentation_url",
                            self.app_documentation_url or "http://www.redtn.cn/documentation/user-manual/index.htm")
        ir_config.set_param("app_documentation_dev_url",
                            self.app_documentation_dev_url or "http://www.redtn.cn/documentation/development/index.htm")
        ir_config.set_param("app_support_url", self.app_support_url or "http://www.redtn.cn/")

