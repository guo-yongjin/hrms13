odoo.define('general_customize.UserMenu', function (require) {
    "use strict";

    /**
     * This widget is appended by the webclient to the right of the navbar.
     * It displays the avatar and the name of the logged user (and optionally the
     * db name, in debug mode).
     * If clicked, it opens a dropdown allowing the user to perform actions like
     * editing its preferences, accessing the documentation, logging out...
     */

    var UserMenu = require('web.UserMenu');
    //避免错误，要再定义
    var documentation_url = 'http://www.redtn.cn';
    var documentation_dev_url = 'http://www.redtn.cn';
    var support_url = 'http://www.redtn.cn';

    UserMenu.include({
        init: function () {
            this._super.apply(this, arguments);
            var self = this;
            var session = this.getSession();


            //取参数
            self._rpc({
                model: 'ir.config_parameter',
                method: 'search_read',
                domain: [['key', '=like', 'app_%']],
                fields: ['key', 'value'],
                lazy: false,
            }).then(function (res) {
                $.each(res, function (key, val) {
                    if (val.key == 'app_documentation_url')
                        documentation_url = val.value;
                    if (val.key == 'app_documentation_dev_url')
                        documentation_dev_url = val.value;
                    if (val.key == 'app_support_url')
                        support_url = val.value;
                });
                 $('[data-menu="account"]').hide();
                 $('.o_sub_menu_footer').hide();
               
            })
        },
        _onMenuDocumentation: function () {
            window.open(documentation_url, '_blank');
        },
        _onMenuSupport: function () {
            window.open(support_url, '_blank');
        },
        _onMenuDocumentation_dev: function () {
            window.open(documentation_dev_url, '_blank');
        },
    })

    



});
