odoo.define('redtn_hrms.image_widgets', function (require) {
'use strict';

//var core = require('web.core');

//var ImageWidget = require('web_editor.widget').ImageWidget;
var ImageWidget = require('wysiwyg.widgets.media').ImageWidget;
//var QWeb = core.qweb;


    ImageWidget.include({
        xmlDependencies: ImageWidget.prototype.xmlDependencies.concat(
            ['/redtn_hrms/static/src/xml/editor_widget.xml']
        ),
    });
});

