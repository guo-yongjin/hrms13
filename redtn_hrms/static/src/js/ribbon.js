odoo.define('redtn_hrms.ribbon', function(require) {
"use strict";
    var core = require('web.core');
    var _t = core._t;
    var WebRibbon = require('web.ribbon');

    WebRibbon.include({
        init: function (parent, data, options) {
            this._super.apply(this, arguments);
            if (options.attrs.text == 'Archived'){
                this.text ='已存档';
            }
            else if (options.attrs.text == 'Paid'){
                this.text ='已支付';
            }
            else if (options.attrs.text == 'In Payment'){
                this.text ='付款';
            }
            else if (options.attrs.text == 'Lost'){
                this.text ='失去';
            }
            else if (options.attrs.text == 'Won'){
                this.text ='赢得';
            }
            else {
                this.text = options.attrs.text;
            }
            this.bgColor = options.attrs.bg_color;
        }
 });
});
