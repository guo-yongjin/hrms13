odoo.define('redtn_hrms.view_newalloc', function(require) {
    'use strict';

    var core = require('web.core');
    //var QWeb = core.qweb;
    var TimeOffCalendarController= require('hr_holidays.dashboard.view_custo').TimeOffCalendarController;
    var _t = core._t;

    //var TimeOffCalendarController = TimeOffCalendarController.time_off_calendar;

    TimeOffCalendarController.include({
        
        /**
         * Action: create a new allocation request
         *
         * @private
         */
        _onNewAllocation: function () {
            var self = this;
            this.do_action({
                type: 'ir.actions.act_window',
                res_model: 'hr.leave.allocation',
                name: _t('New Allocation Request'),
                views: [[false,'form']],
                context: {'form_view_ref': 'hr_holidays.hr_leave_allocation_view_form_dashboard'},
                target: 'new',
            }, {
                on_close: function () {
                    self.reload();
                }
            });
        },
    });
});
