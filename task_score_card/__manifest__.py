# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2019 RedTN
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################

{
    'name': '任务计分',
    'version': '13.0.2',
    'category': 'Project',
    'description': """  系统根据任务开始日期和结束日期提供任务分数 """,
    'author': 'guo yongjin',
    'website': 'http://www.redtn.cn',
    'depends': [
         'project',
        ],
    'data': [
        "views/task_score_card_view.xml",
        ],
    'images': [
        'static/description/banner.png',
        'static/description/icon.png',
        'static/description/index.html',
    ],
    'installable': True,
    'active': False,
    'application': False,

}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
