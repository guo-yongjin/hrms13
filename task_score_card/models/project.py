# -*- coding: utf-8 -*-
###################################################################################
#   
#    Copyright (C) 2019 RedTN 
# 
#此程序是自由软件：您可以根据自由软件基金会发布的GNU Affero通用公共许可证（许可证的第3版或（由您选择）任何更高版本）的条款重新分发和/或修改它。
#这个程序是分发的，希望它是有用的，但没有任何保证；甚至没有隐含的保证适销性或适合特定用途。有关详细信息，请参阅GNU Affero通用公共许可证。
#
#您应该已经收到GNU Affero通用公共许可证的副本以及此程序。如果没有，请参见<http://www.gnu.org/licenses/>。#
###################################################################################

from odoo import api, fields, models,_
from datetime import datetime,date
import time
from odoo.exceptions import UserError

class Task(models.Model):
    _inherit = "project.task"
    _description = "Task"

    @api.model
    def get_delay_days(self):
        res = {}
        for record in self:
            delay_days = False
            if record.date_deadline:
                # if (record.date_deadline > datetime.strptime(time.strftime("%Y-%m-%d"), "%Y-%m-%d").date()):
                #     delay_days = 0
                # elif (record.date_deadline < datetime.strptime(time.strftime("%Y-%m-%d"), "%Y-%m-%d").date()):
                #     delay_days = (record.date_deadline - datetime.strptime(time.strftime("%Y-%m-%d"), "%Y-%m-%d").date()).days
                if (record.date_deadline > date.today()):
                    delay_days = 0
                elif (record.date_deadline < date.today()):
                    delay_days = (record.date_deadline - date.today()).days
            record.delay_days = abs(delay_days)

    @api.model
    def get_points(self):
        res = {}
        for val in self:
            points = 0
            if val.delay_days < 10:
                points = 100 - (val.delay_days * 10)
            elif val.delay_days >= 10 and val.delay_days <= 14:
                points = 0
            elif val.delay_days >= 15 and val.delay_days <= 19:
                points = -50
            elif val.delay_days >= 20:
                points = -100
            val.points = points

    delay_days = fields.Integer(compute='get_delay_days', string='Days Delayed')
    points = fields.Integer(compute='get_points', string='Points')
    date_assign = fields.Datetime(string='Assigning Date', index=True, copy=False, readonly=True)

    @api.model
    def create(self, vals):
        res = super(Task, self).create(vals)
        if not vals.get('date_assign'):
            #raise UserError(_("Kindly fill out all the fields."))
            vals['date_assign'] = fields.Datetime.now()
        if not vals.get('date_deadline'):
            #raise UserError(_("kindly fill out all the fields."))
            vals['date_deadline'] = fields.Datetime.now()
        return res

    
    def write(self, vals):
        res = super(Task, self).write(vals)
        for dt in self:
            if not dt.date_assign:
                raise UserError(_("Kindly give the Assigning date for the task."))
            if not dt.date_deadline:
                raise UserError(_("Kindly give the deadline for the task"))
            if dt.date_assign.date() > dt.date_deadline:
                raise UserError(_("Date Deadline should not be less than the Assigned Date."))
        return res
